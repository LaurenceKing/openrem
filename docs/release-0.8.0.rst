########################
Upgrade to OpenREM 0.8.0
########################

****************
Headline changes
****************

* Charts: Added mammography scatter plot, thanks to `@rijkhorst`_

***************************************************
Upgrading an OpenREM server with no internet access
***************************************************


****************************
Upgrading from version 0.7.4
****************************

* Set the date format for xlsx exports (need to check csv situation). Copy the following code into your
  `local_settings.py` file if you want to change it from dd/mm/yyy:

.. sourcecode:: python

    # Date format for exporting data to Excel xlsx files.
    # Default in OpenREM is dd/mm/yyyy. Override it by uncommenting and customising below; a full list of codes is available
    # at https://msdn.microsoft.com/en-us/library/ee634398.aspx.
    # XLSX_DATE = 'mm/dd/yyyy'



..  _@rijkhorst: https://bitbucket.org/rijkhorst/